#!/usr/bin/perl

my $toReplace1 = '
	<pluginRepositories>
        <pluginRepository>
            <id>public</id>
            <name>Plugin Repository Proxy</name>
            <url>https://artifacts.opsecol.net/repository/maven-public/</url>
        </pluginRepository>
    </pluginRepositories>
    ';

my $toReplace2 = '
    <repositories>
        <repository>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <releases>
                <enabled>true</enabled>
            </releases>
            <id>central-public</id>
            <name>Maven Central Repository Proxy</name>
            <url>https://artifacts.opsecol.net/repository/maven-public/</url>
            <layout>default</layout>
        </repository>
        <repository>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <releases>
                <enabled>true</enabled>
            </releases>
            <id>legacy</id>
            <name>MM Legacy Repository Proxy</name>
            <url>https://artifacts.opsecol.net/repository/mm-releases-legacy-proxy/</url>
            <layout>default</layout>
        </repository>
        <repository>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <releases>
                <enabled>true</enabled>
            </releases>
            <id>mm-releases</id>
            <name>MM 2016 Repository Proxy</name>
            <url>https://artifacts.opsecol.net/repository/mm-releases-proxy/</url>
            <layout>default</layout>
        </repository>
        <repository>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <releases>
                <enabled>true</enabled>
            </releases>
            <id>mm-snapshots</id>
            <name>MM 2016 Snapshots Proxy</name>
            <url>https://artifacts.opsecol.net/repository/mm-snapshots-proxy/</url>
            <layout>default</layout>
        </repository>
    </repositories>
    ';

my $toReplace3 = '
    <distributionManagement>
        <repository>
            <id>Releases</id>
            <name>release repo</name>
            <url>https://artifacts.opsecol.net/repository/Releases/</url>
        </repository>
        <snapshotRepository>
            <id>Snapshots</id>
            <name>snapshot repo</name>
            <url>https://artifacts.opsecol.net/repository/Snapshots/</url>
        </snapshotRepository>
    </distributionManagement>
    ';

#open(in, '<', "/Users/ralphfernandes/Documents/pom.xml") or die("error opening pom.xml: ");

while(<STDIN>) {
	if(/<pluginRepositories>/../<\/pluginRepositories>/) {
		if(/<pluginRepositories>/) {
		}
		if(/<\/pluginRepositories>/) {
			print $toReplace1;
		}
	} elsif(/<repositories>/../<\/repositories>/) {
		if(/<repositories>/) {
		}
		if(/<\/repositories>/) {
			print $toReplace2;
		}
	} elsif (/<distributionManagement>/../<\/distributionManagement>/) {
		if(/<distributionManagement>/) {
		}
		if(/<\/distributionManagement>/) {
			print $toReplace3;
		}
	} elsif (/docker.mm-corp.net:5000/) {
		s/docker.mm-corp.net:5000/docker.opsecol.net/;
		print;
	}
	 else {
		print;
	}
	
}

#close(in);

